package generica.core;

import feign.Headers;
import feign.Param;
import feign.QueryMap;
import feign.RequestLine;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface EntityClient<D extends Dto> {
    @RequestLine("GET /")
    PageDto<D> findAll(@QueryMap Map<String, ?> pageInfo);

    default PageDto<D> findAll(Pageable pageable) {
        return findAll(pageableToMap(pageable));
    }

    @RequestLine("GET /?all=all")
    List<D> findAllUnpaged();

    @RequestLine("GET /{id}")
    D findOne(@Param("id") Long id);

    @RequestLine("PUT /{id}")
    @Headers("Content-type: application/json")
    D update(@Param("id") Long id, D dto);

    @RequestLine("POST /")
    @Headers("Content-type: application/json")
    D create(D dto);

    @RequestLine("GET /search?q={q}&page={page}")
    PageDto<D> search(@Param("q") String query, @Param("page") int page);

    @RequestLine("GET /byIds?ids={ids}")
    List<D> findMany(@Param(value = "ids") Set<Long> ids);

    private static Map<String, ?> pageableToMap(Pageable pageable) {
        return Map.of("page", pageable.getPageNumber());
    }
}
