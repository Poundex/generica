package generica.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PageDto<D extends Dto> implements Dto {
    private Collection<D> results;
    private int currentPage;
    private int totalPages;
    private int itemCount;
    private long totalItemCount;
    private boolean hasNext;
    private boolean hasPrevious;
}
