package net.poundex.generica.core.web.dto;

import lombok.Value;

import java.util.Set;

@Value
public class ValidationFailedException extends RuntimeException {
    Set<ValidationErrorDto> validationErrors;
}
