package net.poundex.generica.core.web.dto;

import generica.core.Dto;
import generica.core.MapWith;
import lombok.Data;
import lombok.Setter;

@Data
public class ValidationErrorDto implements Dto {
    private String message;
    private String messageTemplate;
    @Setter(onMethod = @__({@MapWith("toString()")}))
    private String propertyPath;
}
