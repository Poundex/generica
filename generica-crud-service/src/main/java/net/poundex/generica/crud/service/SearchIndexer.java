package net.poundex.generica.crud.service;

import lombok.RequiredArgsConstructor;
import org.hibernate.search.mapper.orm.Search;
import org.springframework.boot.CommandLineRunner;

import javax.persistence.EntityManagerFactory;

@RequiredArgsConstructor
public class SearchIndexer implements CommandLineRunner {

    private final EntityManagerFactory entityManagerFactory;
    private final SearchConfigurationService searchConfigurationService;

    @Override
    public void run(String... args) throws Exception {
        Search.session(entityManagerFactory.createEntityManager())
                .massIndexer(searchConfigurationService.getSearchableEntities())
                .startAndWait();
    }
}
