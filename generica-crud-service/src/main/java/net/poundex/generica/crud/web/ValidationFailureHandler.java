package net.poundex.generica.crud.web;

import net.poundex.generica.core.web.dto.ValidationErrorDto;
import net.poundex.generica.crud.DtoMapper;
import org.springframework.data.util.Streamable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;

@ControllerAdvice
public class ValidationFailureHandler {
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<?> handleValidationFailure(ConstraintViolationException cvex) {
        return ResponseEntity.unprocessableEntity().body(
                DtoMapper.toDtos(Streamable.of(cvex.getConstraintViolations()), ValidationErrorDto.class));
    }
}
