package net.poundex.generica.crud.web;

import generica.crud.domain.AbstractEntity;

public class UnresolvableRelatedEntityException extends RuntimeException {
    public UnresolvableRelatedEntityException(AbstractEntity entity, Class<? extends AbstractEntity> unresolvableType, Long unresolvableId) {
        super(String.format("Entity %s has dependency of type %s and ID %s that could not be found",
                entity, unresolvableType.getSimpleName(), unresolvableId));
    }
}
