package net.poundex.generica.crud.search;

import org.hibernate.search.backend.lucene.analysis.LuceneAnalysisConfigurationContext;

public interface LuceneContextConfigurer {
    void configureContext(LuceneAnalysisConfigurationContext context);
}
