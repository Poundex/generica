package net.poundex.generica.crud;

import generica.core.Dto;
import generica.core.MapWith;
import generica.core.PageDto;
import io.vavr.Tuple;
import io.vavr.collection.Stream;
import io.vavr.control.Try;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class DtoMapper {
    public static <E, D extends Dto> List<D> toDtos(java.util.stream.Stream<E> entities, Class<D> dtoClass) {
        return entities.map(e -> toDto(e, dtoClass)).collect(Collectors.toList());
    }

    public static <E, D extends Dto> PageDto<D> toDtos(Page<E> page, Class<D> dtoClass) {
        return new PageDto<>(toDtos(page.getContent(), dtoClass), page.getNumber(), page.getTotalPages(), page.getContent().size(), page.getTotalElements(), page.hasNext(), page.hasPrevious());
    }

    public static <E, D extends Dto> List<D> toDtos(Iterable<E> entities, Class<D> dtoClass) {
        return toDtos(StreamSupport.stream(entities.spliterator(), false),  dtoClass);
    }

    public static <E, D extends Dto> D toDto(E entity, Class<D> dtoClass) {
        if(entity == null)
            return null;

        return Binder.bind(entity, dtoClass);
    }

    private static class Binder<E, D extends Dto> {

        private static final ExpressionParser expressionParser = new SpelExpressionParser();

        public static <E, D extends Dto> D bind(E entity, Class<D> dtoClass) {
            return Try.of(() -> new Binder<E, D>(entity, dtoClass).doBind())
                    .getOrElseThrow(DtoMappingFailureException::new);
        }

        private final E sourceEntity;
        private final Class<D> dtoClass;

        private D dto;
        private Map<String, PropertyDescriptor> readableSourceProperties;
        private Map<String, PropertyDescriptor> writableDestinationProperties;

        private Binder(E sourceEntity, Class<D> dtoClass) {
            this.sourceEntity = sourceEntity;
            this.dtoClass = dtoClass;
        }

        private D doBind() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
            dto = dtoClass.getDeclaredConstructor().newInstance();
            readableSourceProperties = getSourceProperties();
            writableDestinationProperties = getDestinationProperties();
            bindProperties();
            return dto;
        }

        private Map<String, PropertyDescriptor> getSourceProperties() {
            return Stream.of(BeanUtils.getPropertyDescriptors(sourceEntity.getClass()))
                    .filter(pd -> pd.getReadMethod() != null)
                    .toJavaMap(pd -> Tuple.of(pd.getName(), pd));
        }

        private Map<String, PropertyDescriptor> getDestinationProperties() {
            return Stream.of(BeanUtils.getPropertyDescriptors(dtoClass))
                    .filter(pd -> pd.getWriteMethod() != null)
                    .toJavaMap(pd -> Tuple.of(pd.getName(), pd));
        }

        private void bindProperties() {
            readableSourceProperties.forEach((srcPropName, srcPd) ->
                    Optional.ofNullable(writableDestinationProperties.get(srcPropName))
                            .ifPresent(dstPd -> bindProperty(srcPd, dstPd)));
        }

        private void bindProperty(PropertyDescriptor sourceProp, PropertyDescriptor destProp) {
            Try.of(() -> sourceProp.getReadMethod().invoke(sourceEntity))
                    .toJavaOptional()
                    .ifPresent(sourceValue -> {
                        if (Dto.class.isAssignableFrom(destProp.getPropertyType()))
                            bindDtoProperty(sourceValue, destProp);
                        else if (isDtoCollection(destProp))
                            bindCollectionOfDtosProperty(sourceValue, destProp);
                        else
                            bindSimpleProperty(sourceValue, destProp);
                    });
        }

        @SuppressWarnings("unchecked")
        private <E1, D1 extends Dto> void bindCollectionOfDtosProperty(Object sourceValue, PropertyDescriptor destProp) {
            Try.run(() -> destProp.getWriteMethod().invoke(dto,
                    writeCollection(
                            (Class<D1>) getCollectionType(destProp),
                            (Class<? extends Collection<?>>) destProp.getReadMethod().getReturnType(),
                            (Collection<E1>) sourceValue))).get();
        }

        private boolean isDtoCollection(PropertyDescriptor sourceProp) {
            return Collection.class.isAssignableFrom(sourceProp.getPropertyType()) &&
                    Dto.class.isAssignableFrom(getCollectionType(sourceProp));
        }

        private Class<?> getCollectionType(PropertyDescriptor propertyDescriptor) {
            return ((Class<?>) ((ParameterizedType) propertyDescriptor.getReadMethod().getGenericReturnType()).getActualTypeArguments()[0]);
        }

        @SuppressWarnings("unchecked")
        private <E1, D1 extends Dto> void bindDtoProperty(Object sourceValue, PropertyDescriptor destProp) {
            Try.run(() -> destProp.getWriteMethod().invoke(dto,
                    toDto((E1) sourceValue,
                            (Class<D1>) destProp.getReadMethod().getReturnType())))
                    .get();
        }

        private void bindSimpleProperty(Object sourceValue, PropertyDescriptor destProp) {
            Try.run(() -> destProp.getWriteMethod().invoke(dto,
                    Optional.ofNullable(destProp.getWriteMethod().getAnnotation(MapWith.class))
                            .map(MapWith::value)
                            .map(r -> expressionParser
                                    .parseExpression(r)
                                    .getValue(sourceValue))
                            .orElse(sourceValue)))
                    .get();
        }

        private <E1, D1 extends Dto> Collection<D1> writeCollection(Class<D1> dtoClass, Class<? extends Collection<?>> collectionClass, Collection<E1> sourceCollection) {
            java.util.stream.Stream<D1> s = sourceCollection.stream()
                    .map(e -> toDto(e, dtoClass));

            if(List.class.isAssignableFrom(collectionClass))
                return s.collect(Collectors.toList());
            else
                return s.collect(Collectors.toSet());
        }
    }

    public static class DtoMappingFailureException extends RuntimeException {
        public DtoMappingFailureException(Throwable cause) {
            super(cause);
        }
    }

}
