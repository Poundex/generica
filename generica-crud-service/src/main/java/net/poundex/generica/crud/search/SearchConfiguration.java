package net.poundex.generica.crud.search;

import generica.crud.search.Field;
import lombok.Value;

import java.util.Map;
import java.util.stream.Collectors;

@Value
public class SearchConfiguration {
    Class<?> entityClass;
    Map<String, Float> fields;

    public String[] getFields() {
        return fields.keySet().toArray(new String[0]);
    }

    public Map<String, Float> getBoostedFields() {
        return fields.entrySet().stream()
                .filter(e -> e.getValue() != Field.NO_BOOST)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
