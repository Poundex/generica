package net.poundex.generica.crud.service;

import generica.crud.search.Field;
import generica.crud.search.Searchable;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.control.Option;
import lombok.RequiredArgsConstructor;
import net.poundex.generica.crud.search.SearchConfiguration;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;
import javax.persistence.metamodel.Type;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SearchConfigurationService {

    private final EntityManagerFactory entityManagerFactory;
    private final Map<Class<?>, SearchConfiguration> searchConfigurations = new HashMap<>();

    @PostConstruct
    public void init() {
        searchConfigurations.putAll(
                entityManagerFactory.createEntityManager().getMetamodel().getEntities().stream()
                        .map(Type::getJavaType)
                        .map(c -> Tuple.of(c, c.getAnnotation(Searchable.class)))
                        .filter(cs -> cs._2() != null)
                        .collect(Collectors.toMap(Tuple2::_1, cs ->
                                new SearchConfiguration(cs._1(), Arrays.stream(cs._2().fields())
                                        .collect(Collectors.toMap(Field::value, Field::boost))))));
    }

    public Collection<? extends Class<?>> getSearchableEntities() {
        return searchConfigurations.keySet();
    }

    public Option<SearchConfiguration> getSearchConfigFor(Class<?> klass) {
        return Option.of(searchConfigurations.get(klass));
    }
}
