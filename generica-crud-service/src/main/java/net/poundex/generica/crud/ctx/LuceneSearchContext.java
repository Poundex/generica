package net.poundex.generica.crud.ctx;

import lombok.RequiredArgsConstructor;
import org.hibernate.search.backend.lucene.analysis.LuceneAnalysisConfigurationContext;
import org.hibernate.search.backend.lucene.analysis.LuceneAnalysisConfigurer;
import org.springframework.context.annotation.Configuration;

import java.util.Collection;

@Configuration("luceneAnalysisConfigurer")
@RequiredArgsConstructor
public class LuceneSearchContext implements LuceneAnalysisConfigurer {

    private final Collection<LuceneAnalysisConfigurer> luceneAnalysisConfigurers;

    @Override
    public void configure(LuceneAnalysisConfigurationContext context) {
        luceneAnalysisConfigurers.forEach(c -> c.configure(context));
    }
}
