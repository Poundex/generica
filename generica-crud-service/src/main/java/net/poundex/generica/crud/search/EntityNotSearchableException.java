package net.poundex.generica.crud.search;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class EntityNotSearchableException extends RuntimeException {
    private final Class<?> entityClass;
}
