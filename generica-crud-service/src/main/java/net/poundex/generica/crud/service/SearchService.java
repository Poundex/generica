package net.poundex.generica.crud.service;

import io.vavr.Tuple;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import net.poundex.generica.crud.search.EntityNotSearchableException;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.search.Query;
import org.hibernate.search.backend.lucene.LuceneExtension;
import org.hibernate.search.engine.search.query.SearchFetchable;
import org.hibernate.search.engine.search.query.dsl.SearchQueryOptionsStep;
import org.hibernate.search.mapper.orm.Search;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SearchService {

    private final SearchConfigurationService searchConfigurationService;
    private final ObjectFactory<EntityManager> entityManagerFactory;

    public <E> Try<List<E>> search(Class<E> klass, String query) {
        return configureQuery(klass, query).map(SearchFetchable::fetchAllHits);
    }

    public <E> Try<Page<E>> search(Class<E> klass, String query, Pageable pageable) {
        Pageable actualPageable = pageable != null ? pageable : Pageable.unpaged();
        return configureQuery(klass, query)
                .map(q -> q.fetch(actualPageable.getPageNumber() * actualPageable.getPageSize(),
                        actualPageable.getPageSize()))
                .map(r -> Tuple.of(r.hits(), r.totalHitCount()))
                .map(r -> new PageImpl<>(r._1(), actualPageable, r._2()));
    }

    private <E> Try<SearchQueryOptionsStep<?, E, ?, ?, ?>> configureQuery(Class<E> klass, String query) {
        return searchConfigurationService.getSearchConfigFor(klass)
                .toTry(() -> new EntityNotSearchableException(klass))
                .mapTry(config -> {
                    Query q = new MultiFieldQueryParser(config.getFields(), new StandardAnalyzer(), config.getBoostedFields())
                            .parse(query);

                    return Search.session(entityManagerFactory.getObject())
                            .search(klass)
                            .extension(LuceneExtension.get())
                            .where(f -> f.fromLuceneQuery(q));
                });
    }
}
