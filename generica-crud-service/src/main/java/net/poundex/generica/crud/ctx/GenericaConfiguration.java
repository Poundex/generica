package net.poundex.generica.crud.ctx;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"net.poundex.generica", "generica"})
@EntityScan("generica.crud.domain")
public class GenericaConfiguration {
}
