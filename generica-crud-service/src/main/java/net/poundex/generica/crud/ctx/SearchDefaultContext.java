package net.poundex.generica.crud.ctx;

import generica.crud.search.DefaultContext;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.hibernate.search.backend.lucene.analysis.LuceneAnalysisConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SearchDefaultContext {
    @Bean
    public LuceneAnalysisConfigurer standardAnalyzer() {
        return ctx -> ctx.analyzer(DefaultContext.STANDARD_ANALYZER).instance(new StandardAnalyzer());
    }

}
