package generica.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenericaCrudServiceApplication {

	public static void launch(Class<?> applicationConfiguration, String[] args) {
		SpringApplication.run(
				new Class[] { GenericaCrudServiceApplication.class, applicationConfiguration }, args);
	}

	public static void main(String[] args) {
		SpringApplication.run(GenericaCrudServiceApplication.class, args);
	}

}
