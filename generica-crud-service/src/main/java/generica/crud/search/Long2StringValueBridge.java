package generica.crud.search;

import org.hibernate.search.mapper.pojo.bridge.ValueBridge;
import org.hibernate.search.mapper.pojo.bridge.runtime.ValueBridgeFromIndexedValueContext;
import org.hibernate.search.mapper.pojo.bridge.runtime.ValueBridgeToIndexedValueContext;

public class Long2StringValueBridge implements ValueBridge<Long, String> {
    @Override
    public String toIndexedValue(Long value, ValueBridgeToIndexedValueContext context) {
        return value.toString();
    }

    @Override
    public Long fromIndexedValue(String value, ValueBridgeFromIndexedValueContext context) {
        return Long.parseLong(value);
    }
}
