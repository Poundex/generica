package generica.crud.search;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
public @interface Field {
    float NO_BOOST = -9119f;

    String value();
    float boost() default NO_BOOST;
}
