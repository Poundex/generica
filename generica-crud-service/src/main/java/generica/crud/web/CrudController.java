package generica.crud.web;

import generica.core.Dto;
import generica.crud.domain.AbstractEntity;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import net.poundex.generica.crud.DtoMapper;
import net.poundex.generica.crud.search.EntityNotSearchableException;
import net.poundex.generica.crud.service.SearchService;
import net.poundex.generica.crud.web.UnresolvableRelatedEntityException;
import org.apache.lucene.queryparser.classic.ParseException;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.util.Streamable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RequiredArgsConstructor
public abstract class CrudController<E, D extends Dto> {

    protected final PagingAndSortingRepository<E, Long> repository;
    private final Class<D> dtoClass;
    private final Class<E> entityClass;
    private final Validator validator;
    private final SearchService searchService;

    @GetMapping
    public ResponseEntity<?> findAll(Pageable pageable, @RequestParam(required = false) String all) {
        return ResponseEntity.ok("all".equals(all)
                ? DtoMapper.toDtos(repository.findAll(), dtoClass)
                : DtoMapper.toDtos(repository.findAll(pageable), dtoClass));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findOne(@PathVariable Long id) {
        return repository.findById(id)
                .map(e -> DtoMapper.toDto(e, dtoClass))
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody D dto) {
        return repository.findById(id)
                .map(e -> bind(dto, e))
                .stream()
                .peek(this::validate)
                .map(repository::save)
                .map(e -> DtoMapper.toDto(e, dtoClass))
                .map(ResponseEntity::ok)
                .findFirst()
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody D dto) {
        return Try.of(() -> entityClass.getDeclaredConstructor().newInstance())
                .map(e -> bind(dto, e))
                .peek(this::validate)
                .map(repository::save)
                .map(e -> DtoMapper.toDto(e, dtoClass))
                .map(ResponseEntity::ok)
                .get();
    }

    protected abstract E bind(D dto, E entity);

    private void validate(E entity) {
        Stream.of(validator.validate(entity))
                .filter(errors -> ! errors.isEmpty())
                .findFirst()
                .ifPresent(errors -> {
                    throw new ConstraintViolationException(errors);
                });
    }

    @GetMapping("/search")
    public ResponseEntity<?> search(@RequestParam("q") String query, Pageable pageable) {
        return searchService.search(entityClass, query, pageable)
                .map(l -> DtoMapper.toDtos(l, dtoClass))
                .map(ResponseEntity::ok)
                .recover(EntityNotSearchableException.class, x -> ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).build())
                .recover(ParseException.class, x -> ResponseEntity.unprocessableEntity().build())
                .get();
    }

    @GetMapping("/byIds")
    public ResponseEntity<?> findMany(@RequestParam("ids") Set<Long> ids) {
        return ResponseEntity.ok(DtoMapper.toDtos(repository.findAllById(ids), dtoClass));
    }

    protected static <D extends Dto, E extends AbstractEntity> E resolveSingle(D dto, CrudRepository<E, Long> repository, AbstractEntity entity, Class<E> entityClass) {
        return Optional.ofNullable(dto)
                .map(r -> repository.findById(r.getId())
                        .orElseThrow(() -> new UnresolvableRelatedEntityException(entity, entityClass, r.getId())))
                .orElse(null);
    }

    protected static <D extends Dto, E extends AbstractEntity> Set<E> resolveMany(Set<D> dtos, CrudRepository<E, Long> repository, AbstractEntity entity, Class<E> entityClass) {
        return new HashSet<>(Streamable.of(repository.findAllById(Optional.ofNullable(dtos)
                .orElse(Collections.emptySet()).stream()
                .map(Dto::getId)
                .collect(Collectors.toSet())))
                .toSet());
    }
}
