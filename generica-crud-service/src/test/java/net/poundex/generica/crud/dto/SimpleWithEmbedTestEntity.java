package net.poundex.generica.crud.dto;

import lombok.Value;

import java.util.List;
import java.util.Set;

@Value
public class SimpleWithEmbedTestEntity {
    String stringProperty;
    SimpleTestEntity embeddedProperty;
    List<SimpleTestEntity> listProperty;
    Set<SimpleTestEntity> setProperty;
}
