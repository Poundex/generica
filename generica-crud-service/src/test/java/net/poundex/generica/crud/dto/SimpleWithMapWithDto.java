package net.poundex.generica.crud.dto;

import generica.core.Dto;
import generica.core.MapWith;
import lombok.Data;
import lombok.Setter;

@Data
public class SimpleWithMapWithDto implements Dto {
    @Setter(onMethod = @__({@MapWith("someMethod()")}))
    private String propertyOne;

    @Setter(onMethod = @__({@MapWith("toString()")}))
    private String propertyTwo;
}
