package net.poundex.generica.crud.dto;

import lombok.Value;

@Value
public class SimpleTestEntity {
    String stringProperty;
    String onlyInSource;
}
