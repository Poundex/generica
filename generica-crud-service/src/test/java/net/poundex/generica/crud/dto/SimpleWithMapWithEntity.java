package net.poundex.generica.crud.dto;

import lombok.Value;

@Value
public class SimpleWithMapWithEntity {

    ComplexType propertyOne;
    ComplexType propertyTwo;

    public static class ComplexType {
        public String someMethod() {
            return "someMethod";
        }

        @Override
        public String toString() {
            return "toString";
        }
    }

}
