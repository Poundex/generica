package net.poundex.generica.crud.dto;

import generica.core.Dto;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class SimpleWithEmbedTestDto implements Dto {
    private String stringProperty;
    private SimpleTestDto embeddedProperty;
    List<SimpleTestDto> listProperty;
    Set<SimpleTestDto> setProperty;
}
