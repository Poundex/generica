package net.poundex.generica.crud.dto;

import generica.core.Dto;
import lombok.Data;

@Data
public class SimpleTestDto implements Dto {
    private String stringProperty;
    private String onlyInDto;
}
