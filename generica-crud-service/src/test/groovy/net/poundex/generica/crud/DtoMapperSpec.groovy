package net.poundex.generica.crud

import net.poundex.generica.crud.dto.SimpleTestDto
import net.poundex.generica.crud.dto.SimpleTestEntity
import net.poundex.generica.crud.dto.SimpleWithEmbedTestDto
import net.poundex.generica.crud.dto.SimpleWithEmbedTestEntity
import net.poundex.generica.crud.dto.SimpleWithMapWithDto
import net.poundex.generica.crud.dto.SimpleWithMapWithEntity
import org.springframework.data.util.Streamable
import spock.lang.Specification

class DtoMapperSpec extends Specification {
    void "Map simple properties"() {
        given:
        SimpleTestEntity entity = new SimpleTestEntity("stringValue", "notSeen")

        when:
        SimpleTestDto dto = DtoMapper.toDto(entity, SimpleTestDto)

        then:
        dto.stringProperty == "stringValue"
    }

    void "Return null for null input"() {
        expect:
        DtoMapper.toDto(null, SimpleTestDto) == null
    }

    void "Map collections"() {
        given:
        List<SimpleTestEntity> entities = (1..5).collect {
            new SimpleTestEntity("stringValue${it}", "notSeen")
        }

        when:
        List<SimpleTestDto> dtos = DtoMapper.toDtos(Streamable.of(entities), SimpleTestDto)

        then:
        dtos.size() == 5
        dtos.every { it != null }
        dtos.every { it instanceof SimpleTestDto }
        (1..5).every { dtos[it - 1].stringProperty == "stringValue${it}" }
    }

    void "Map embedded DTO properties"() {
        given:
        SimpleTestEntity entity1 = new SimpleTestEntity("stringValue", "notSeen")
        SimpleWithEmbedTestEntity entity2 = new SimpleWithEmbedTestEntity("anotherStringProperty", entity1, null, null)

        when:
        SimpleWithEmbedTestDto dto = DtoMapper.toDto(entity2, SimpleWithEmbedTestDto)

        then:
        dto.stringProperty == "anotherStringProperty"
        dto.embeddedProperty
        dto.embeddedProperty.stringProperty == "stringValue"
    }

    void "Map collections of embedded DTO properties"() {
        given:
        SimpleTestEntity entity1 = new SimpleTestEntity("stringValue1", "notSeen")
        SimpleTestEntity entity2 = new SimpleTestEntity("stringValue2", "notSeen")
        SimpleTestEntity entity3 = new SimpleTestEntity("stringValue3", "notSeen")
        SimpleTestEntity entity4 = new SimpleTestEntity("stringValue4", "notSeen")

        SimpleWithEmbedTestEntity entity5 = new SimpleWithEmbedTestEntity("anotherStringProperty", null,
                List.of(entity1, entity2),
                Set.of(entity3, entity4))

        when:
        SimpleWithEmbedTestDto dto = DtoMapper.toDto(entity5, SimpleWithEmbedTestDto)

        then:
        dto.listProperty
        dto.listProperty.size() == 2
        dto.listProperty.every { it instanceof SimpleTestDto }
        dto.listProperty.containsAll([entity1, entity2].collect { DtoMapper.toDto(it, SimpleTestDto)})

        dto.setProperty
        dto.setProperty.size() == 2
        dto.setProperty.every { it instanceof SimpleTestDto }
        dto.setProperty.containsAll([entity3, entity4].collect { DtoMapper.toDto(it, SimpleTestDto)})
    }

    void "Use mappers when supplied"() {
        given:
        SimpleWithMapWithEntity entity = new SimpleWithMapWithEntity(new SimpleWithMapWithEntity.ComplexType(), new SimpleWithMapWithEntity.ComplexType())

        when:
        SimpleWithMapWithDto dto = DtoMapper.toDto(entity, SimpleWithMapWithDto)

        then:
        dto.propertyOne == "someMethod"
        dto.propertyTwo == "toString"
    }
}
