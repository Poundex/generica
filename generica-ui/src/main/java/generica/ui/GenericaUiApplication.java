package generica.ui;

import freemarker.cache.ClassTemplateLoader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.util.Properties;

@SpringBootApplication
@PropertySource(value = "/generica-ui.properties")
public abstract class GenericaUiApplication {

    public static void launch(Class<?> applicationConfiguration, String[] args) {
        SpringApplication.run(
                new Class[] { GenericaUiApplication.class, applicationConfiguration }, args);
    }

    @Bean
    @Primary
    public FreeMarkerConfigurer freeMarkerConfigurer() {
        FreeMarkerConfigurer configurer = new FreeMarkerConfigurer();
        configurer.setPreTemplateLoaders(new ClassTemplateLoader(this.getClass(), "/templates/"));
        Properties p = new Properties();
        p.setProperty("auto_import", "generica/page.ftlh as gp");
        configurer.setFreemarkerSettings(p);
        return configurer;
    }
}
