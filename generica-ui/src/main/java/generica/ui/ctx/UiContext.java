package generica.ui.ctx;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Getter
public class UiContext {
    private final MainMenu mainMenu;
}
