package generica.ui.ctx;

import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MainMenu {
    @Getter
    private final List<AbstractMenuItem> menuItems;

    public MainMenu(AbstractMenuItem... menuItems) {
        this.menuItems = Arrays.asList(menuItems);
    }

    public static abstract class AbstractMenuItem {
        @Getter
        private final String text;

        public AbstractMenuItem(String text) {
            this.text = text;
        }

        public abstract String render();
    }

    public static class MenuItem extends AbstractMenuItem {
        @Getter
        private final String href;
        @Getter
        private final String navKey;

        public MenuItem(String text, String href, String navKey) {
            super(text);
            this.href = href;
            this.navKey = navKey;
        }

        @Override
        public String render() {
            return String.format("<@singleMenuItem text=\"%s\" href=\"%s\" forNavKey=\"%s\" />", getText(), getHref(), getNavKey());
        }
    }

    public static class MenuCategory extends AbstractMenuItem {
        @Getter
        private final List<MenuItem> menuItems;

        public MenuCategory(String text, MenuItem... menuItems) {
            super(text);
            this.menuItems = Arrays.asList(menuItems);
        }

        @Override
        public String render() {
            return String.format("<@menuCategory text=\"%s\">%s</@menuCategory>", getText(), menuItems.stream()
                    .map(i -> String.format("<@menuLink text=\"%s\" href=\"%s\" forNavKey=\"%s\" />", i.getText(), i.getHref(), i.getNavKey()))
                    .collect(Collectors.joining("\n")));
        }
    }
}
