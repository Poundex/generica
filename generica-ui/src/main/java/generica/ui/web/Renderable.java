package generica.ui.web;

import generica.ui.ctx.UiContext;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Renderable {
    private final UiContext uiContext;
    private final Map<String, Object> model = new HashMap<>();
    private final List<Message> messages = new LinkedList<>();

    private String viewName;
    private HttpStatus httpStatus = HttpStatus.OK;

    public Renderable(String viewName, UiContext uiContext) {
        this.viewName = viewName;
        this.uiContext = uiContext;
        withModelObject("uiContext", uiContext);
    }

    public Renderable navKey(String navKey) {
        withModelObject("navKey", navKey);
        return this;
    }

    public Renderable withModelObject(String name, Object value) {
        model.put(name, value);
        return this;
    }

    public Renderable httpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
        return this;
    }

    public ModelAndView render() {
        withModelObject("messages", messages);
        return new ModelAndView(viewName, model, httpStatus);
    }

    public Renderable message(MessageType messageType, String message) {
        messages.add(new Message(messageType, message));
        return this;
    }

    public Renderable message(MessageType messageType, String title, String message) {
        messages.add(new Message(messageType,
                String.format("<b>%s</b>\n<br />\n%s", title, message)));
        return this;
    }

    public Renderable view(String name) {
        viewName = name;
        return this;
    }

    @RequiredArgsConstructor
    public enum MessageTypes implements MessageType {
        MESSAGE(""),
        ERROR("error"),
        WARNING("warning"),
        SUCCESS("success"),
        INFORMATION("information"),
        TIP("tip");

        private final String className;

        @Override
        public String getClassName() {
            return className;
        }
    }

    public interface MessageType {
        String getClassName();
    }

    @Value
    public static class Message {
        MessageType messageType;
        String message;
    }
}
