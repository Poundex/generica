package generica.ui.web;

import feign.FeignException;
import generica.core.Dto;
import generica.core.EntityClient;
import generica.core.PageDto;
import generica.ui.ctx.UiContext;
import io.vavr.control.Try;
import lombok.Value;
import net.poundex.generica.core.web.dto.ValidationFailedException;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import static generica.ui.web.Renderable.MessageTypes.ERROR;
import static generica.ui.web.Renderable.MessageTypes.SUCCESS;

public abstract class CrudUiController<D extends Dto, C extends EntityClient<D>> extends UiController {

    private final String templateName;
    protected final EntityClient<D> entityClient;
    private final String navKey;
    private final Class<D> dtoClass;

    public CrudUiController(UiContext uiContext, String templateName, EntityClient<D> entityClient, String navKey, Class<D> dtoClass) {
        super(uiContext);
        this.templateName = templateName;
        this.entityClient = entityClient;
        this.navKey = navKey;
        this.dtoClass = dtoClass;
    }

    protected final static String INDEX_URL_TEMPLATE = "index?page=%s";

    @GetMapping(value = {"", "/", "/index"})
    public ModelAndView findAll(Pageable pageable) {
        return renderEntities(entityClient.findAll(pageable), new PageInfo(
                pageable,
                p -> createLink().resolve(String.format(INDEX_URL_TEMPLATE, pageable.getPageNumber() + 1)).toString(),
                p -> createLink().resolve(String.format(INDEX_URL_TEMPLATE, pageable.getPageNumber() - 1)).toString()
        )).render();
    }

    public Renderable renderEntities(Collection<D> entities) {
        return render(templateName + "/index")
                .navKey(navKey)
                .withModelObject("entities", entities);
    }

    public Renderable renderEntities(PageDto<D> page, PageInfo pageInfo) {
        return renderEntities(page.getResults())
                .withModelObject("isPage", true)
                .withModelObject("pagedItemCount", page.getItemCount())
                .withModelObject("pagedTotalItemCount", page.getTotalItemCount())
                .withModelObject("pagedCurrentPage", page.getCurrentPage() + 1)
                .withModelObject("pagedTotalPages", page.getTotalPages())
                .withModelObject("pagedHasNext", page.isHasNext())
                .withModelObject("pagedHasPrevious", page.isHasPrevious())
                .withModelObject("nextPageUrl", pageInfo.getNextPageUrlRenderer().apply(page))
                .withModelObject("prevPageUrl", pageInfo.getPrevPageUrlRenderer().apply(page));
    }

    @GetMapping("/{id}")
    public ModelAndView findOne(@PathVariable Long id) {
        return renderEntity(entityClient.findOne(id)).render();
    }

    public Renderable renderEntity(D entity) {
        return render(templateName + "/view")
                .navKey(navKey)
                .withModelObject("entity", entity);
    }

    @GetMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable Long id) {
        return renderEditForm(entityClient.findOne(id)).render();
    }

    public Renderable renderEditForm(D entity) {
        return render(templateName + "/edit")
                .navKey(navKey)
                .withModelObject("entity", entity);
    }

    @PostMapping("/save")
    public ModelAndView save(D dto) {
        return Try.of(() ->
                dto.getId() == null
                        ? entityClient.create(dto)
                        : entityClient.update(dto.getId(), dto))
                .map(this::renderEntity)
                .peek(r -> r.message(SUCCESS, "Saved"))
                .recover(ValidationFailedException.class, ex -> {
                    Renderable r = renderEditForm(dto);
                    ex.getValidationErrors().forEach(err ->
                            r.message(ERROR, String.format("%s %s", err.getPropertyPath(), err.getMessage())));
                    return r;
                })
                .get()
                .render();
    }

    @GetMapping("/create")
    public ModelAndView create() {
        return Try.of(() -> dtoClass.getDeclaredConstructor().newInstance())
                .map(this::renderEditForm)
                .get()
                .render();
    }

    protected final static String SEARCH_URL_TEMPLATE = "search?page=%s&q=%s";

    @GetMapping("/search")
    public ModelAndView search(@RequestParam("q") String query, Pageable pageable) {
        if( ! StringUtils.hasText(query))
            return findAll(pageable);

        return renderSearch(query, new PageInfo(
                pageable,
                p -> createLink().resolve(String.format(SEARCH_URL_TEMPLATE, p.getCurrentPage() + 1, query)).toString(),
                p -> createLink().resolve(String.format(SEARCH_URL_TEMPLATE, p.getCurrentPage() - 1, query)).toString()
        )).render();
    }

    protected Renderable renderSearch(String query, PageInfo pageInfo) {
        return Try.of(() -> entityClient.search(query, pageInfo.getPageable().getPageNumber()))
                .map(p -> renderEntities(p, pageInfo))

                .recover(FeignException.UnprocessableEntity.class, x ->
                        this.renderEntities(Collections.emptyList())
                                .message(ERROR, "Search query not parsable"))

                .recover(FeignException.NotImplemented.class, x ->
                        this.renderEntities(Collections.emptyList())
                                .message(ERROR, "Not searchable"))

                .map(r -> r.withModelObject("lastSearch", query))
                .get();
    }

    protected final static String OBJECT_CHOOSER_URL_TEMPLATE = "objectChooser?form=%s&selected=%s&page=%s%s";

    @GetMapping("/objectChooser")
    public ModelAndView objectChooser(
            @RequestParam("form") String formId,
            @RequestParam(value = "q", required = false) String query,
            @RequestParam("selected") Set<Long> selected,
            Pageable pageable) {
        return (query == null
                    ? renderEntities(entityClient.findMany(selected))
                    : renderSearch(query, new PageInfo(pageable,
                        p -> createLink().resolve(String.format(OBJECT_CHOOSER_URL_TEMPLATE, formId, collectionToUrlString(selected), p.getCurrentPage() + 1, StringUtils.hasText(query) ? "&q=" + query : "")).toString(),
                        p -> createLink().resolve(String.format(OBJECT_CHOOSER_URL_TEMPLATE, formId, collectionToUrlString(selected), p.getCurrentPage() - 1, StringUtils.hasText(query) ? "&q=" + query : "")).toString())))
                .view(templateName + "/objectChooser")
                .withModelObject("contentOnly", true)
                .withModelObject("formId", formId)
                .withModelObject("selected", selected)
                .render();
    }

    @Value
    private static class PageInfo {
        Pageable pageable;
        Function<PageDto<?>, String> nextPageUrlRenderer;
        Function<PageDto<?>, String> prevPageUrlRenderer;
    }

    private static String collectionToUrlString(Collection<?> collection) {
        return collection.stream().map(Object::toString).collect(Collectors.joining(","));
    }

    protected abstract URI createLink();
}
