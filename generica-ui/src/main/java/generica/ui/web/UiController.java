package generica.ui.web;

import generica.ui.ctx.UiContext;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class UiController {

    private final UiContext uiContext;

    protected Renderable render(String viewName) {
        return new Renderable(viewName, uiContext);
    }
}
