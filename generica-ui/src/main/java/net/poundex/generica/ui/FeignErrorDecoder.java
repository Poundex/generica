package net.poundex.generica.ui;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import feign.Response;
import feign.codec.ErrorDecoder;
import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import net.poundex.generica.core.web.dto.ValidationErrorDto;
import net.poundex.generica.core.web.dto.ValidationFailedException;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
@RequiredArgsConstructor
public class FeignErrorDecoder extends ErrorDecoder.Default {

    private final static TypeReference<Set<ValidationErrorDto>> SET_OF_VALIDATION_ERRORS_TYPE = new TypeReference<>() {};

    private final ObjectMapper objectMapper;

    @Override
    public Exception decode(String methodKey, Response response) {
        if(response.status() != 422 || response.request().url().contains("/search"))
            return super.decode(methodKey, response);

        return Try.of(() -> new ValidationFailedException(
                objectMapper.readValue(response.body().asInputStream(),
                        SET_OF_VALIDATION_ERRORS_TYPE))).get();
    }
}
