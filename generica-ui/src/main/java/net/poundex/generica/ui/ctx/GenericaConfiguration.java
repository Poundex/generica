package net.poundex.generica.ui.ctx;

import generica.core.Dto;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.control.Option;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.convert.converter.ConverterFactory;
import org.springframework.format.FormatterRegistry;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@ComponentScan(basePackages = {"net.poundex.generica", "generica"})
public class GenericaConfiguration implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverterFactory(new ConverterFactory<Object, Dto>() {
            @Override
            public <T extends Dto> Converter<Object, T> getConverter(Class<T> targetType) {
                return source -> Option.of(getLong(source))
                        .toTry()
                        .mapTry((id) -> Tuple.of(id, targetType.getDeclaredConstructor().newInstance()))
                        .peek(t -> t._2().setId(t._1()))
                        .map(Tuple2::_2)
                        .getOrElse((T) null);
            }

            private Long getLong(Object object) {
                if(object == null)
                    return null;
                if(object instanceof Long num)
                    return num;
                String s = object.toString();
                if( ! StringUtils.hasText(s))
                    return null;
                return Long.parseLong(s);
            }
        });
    }
}
