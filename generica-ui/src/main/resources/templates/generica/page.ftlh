<#macro page title>
    <#if !contentOnly!false>
        <!doctype html>
        <!--[if lt IE 7 ]>
        <html lang="en" class="no-js ie6"> <![endif]-->
        <!--[if IE 7 ]>
        <html lang="en" class="no-js ie7"> <![endif]-->
        <!--[if IE 8 ]>
        <html lang="en" class="no-js ie8"> <![endif]-->
        <!--[if IE 9 ]>
        <html lang="en" class="no-js ie9"> <![endif]-->
        <!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->

        <head>
            <title>${title}</title>

            <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
            <meta name="apple-mobile-web-app-capable" content="yes">
            <meta name="apple-touch-fullscreen" content="yes">
            <meta charset="UTF-8"/>

            <link rel="stylesheet" type="text/css" href="/stylesheets/flakes.css">

            <style type="text/css">
                form li {
                    margin-bottom: 10px;
                }

                form label:first-child {
                    font-weight: bold;
                    display: block;
                }

                .formContainer {
                    margin-bottom: 0.5em;
                }

                .right {
                    float: right;
                }

                .left {
                    float: left;
                }

                .pagination-panel {
                    zoom: 1;
                    clear: both;
                    background: #f9f9f9;
                    border: 1px solid #eeeeee;
                    border-top-right-radius: 0;
                    border-bottom-right-radius: 2px;
                    border-bottom-left-radius: 2px;
                    border-top-left-radius: 0;
                    margin-bottom: 20px;
                    padding: 5px;
                }


            </style>
        </head>

        <body>
        <!--[if lt IE 7]>
        <p class="chromeframe" style="background:#eee; padding:10px; width:100%">Your browser is <em>ancient!</em> <a
                href="http://browsehappy.com/">Upgrade to a different browser</a> or <a
                href="http://www.google.com/chromeframe/?redirect=true">install Google Chrome Frame</a> to experience
            this site.</p>
        <![endif]-->

        <div class="flakes-frame">

        <#include "mainMenu.ftlh">

        <div class="flakes-content">

        <div class="flakes-mobile-top-bar">
            <a href="" class="logo-wrap">
                <img src="img/logo.png" height="30px">
            </a>

            <a href="" class="navigation-expand-target">
                <img src="/img/navigation-expand-target.png" height="26px">
            </a>
        </div>

        <div class="view-wrap">
        <h1>${title}</h1>

        <#list messages as message>
            <div class="grid-10 flakes-message ${message.messageType.className}">
                ${message.message}
            </div>
        </#list>
    </#if>

    <#nested />

    <#if !contentOnly!false>
        </div>
        </div>
        </div>

        <div style="display: none" id="formContainer"></div>

        <link rel="stylesheet" type="text/css" href="/stylesheets/gridforms.css">

        <script src="/js/jquery.js"></script>
        <script src="/js/snap.js"></script>
        <script src="/js/responsive-elements.js"></script>
        <script src="/js/gridforms.js"></script>

        <script src="/js/flakes.js"></script>

        <script src="/js/genericPage.js"></script>
        </body>
        </html>
    </#if>
</#macro>

<#macro entityTable cols entities selectionRole>
    <table class="flakes-table <#if selectionRole=="selectObjectFromIndex">indexSelector</#if>">
        <colgroup>
            <col span="1" style="width:20px" />
            <col span="1"  />
        </colgroup>
        <thead>
        <tr>
            <td><input type="checkbox" class="selectorSelectAll"></td>
            <#list cols?keys as col>
                <td>${col}</td>
            </#list>
        </tr>
        </thead>
        <tbody class="list">
        <#list entities as entity>
            <tr>
                <td><input class="${selectionRole}" data-id="${entity.id}" type="checkbox" <#if selected?? && selected?seq_contains(entity.id)>checked="checked"</#if> /></td>
                <#list cols?values as val>
                    <#assign e=entity>
                    <#assign itemTemplate = "${val}" />
                    <td><@itemTemplate?interpret /></td>
                </#list>
            </tr>
        </#list>
        </tbody>
    </table>
</#macro>

<#macro entitySearchBox entityName formConfig inputConfig autofocus=false>
    <div class="flakes-search">
        <form ${formConfig}>
            <input <#if formId??>form="${formId}"</#if> ${inputConfig} class="search-box search" placeholder="Search ${entityName}" <#if autofocus>autofocus</#if> value="${lastSearch!}">
        </form>
    </div>
</#macro>

<#macro entitySearch entityName resourceName>
    <@entitySearchBox entityName=entityName formConfig="method=\"get\" action=\"/${resourceName}/search\"" inputConfig="id=\"q\" name=\"q\"" autofocus=true />
</#macro>

<#macro objectChooserSearch entityName resourceName>
    <@entitySearchBox entityName=entityName formConfig="data-res=\"${resourceName}\" class=\"objectChooserForm\"" inputConfig="id=\"q\" name=\"q\"" />
</#macro>

<#macro entityListActionBar entityName resource>
    <div class="flakes-actions-bar">
        <a class="action button-gray smaller right" href="/${resource}/create">Create ${entityName}</a>
<#--        <a class="action button-gray smaller right" href="">Export</a>-->

        <button data-enabledWhen="s.size != 0" data-doWithSelection="location.href='/${resource}/delete?ids=' + Array.from(s).join(',')" class="action button-gray smaller selectionSensitiveButton">Delete</button>
        <button data-enabledWhen="s.size == 1" data-doWithSelection="location.href = '/${resource}/edit/' + s.values().next().value" class="action button-gray smaller selectionSensitiveButton">Edit</button>
    </div>
</#macro>

<#macro paginator isObjectChooser=false>
    <#if isPage??>
        <br />
        <div class="pagination-panel">
            <div class="flakes-pagination left">
                Showing ${pagedItemCount} of  ${pagedTotalItemCount}
            </div>
            <div class="flakes-pagination right">
                <#if pagedHasPrevious>
                    <a <#if isObjectChooser>class="objectChooserPageLink"</#if> href="${prevPageUrl}">Prev</a>
                </#if>&nbsp; ${pagedCurrentPage} <i>of</i> ${pagedTotalPages} &nbsp;
                <#if pagedHasNext>
                    <a <#if isObjectChooser>class="objectChooserPageLink"</#if> href="${nextPageUrl}">Next</a>
                </#if>
            </div>
            <br />
            <br />
        </div>
    </#if>
</#macro>

<#macro entityActionBar resource id>
    <div class="flakes-actions-bar">
        <a class="action button-gray smaller" href="/${resource}/edit/${id}">Edit</a>
        <a class="action button-red smaller" href="/${resource}/delete/${id}">Delete</a>
    </div>
</#macro>

<#macro editEntityActionBar resource create form>
    <div class="flakes-actions-bar">
        <button class="action button-green smaller" onclick="document.forms['${form}'].submit()">Save</button>
        <#if !create>
            <button class="action button-gray smaller" onclick="document.forms['${form}'].reset()">Reset</button>
        </#if>
    </div>
</#macro>

<#macro singleEntityChooser name entities blankOption=false selected=-1>
    <select id="${name}" name="${name}">
        <#if blankOption>
            <option value="">&nbsp;</option>
        </#if>
        <#list entities as entity>
            <option value="${entity.id}" <#if ((selected)!-1)==(entity.id)!>selected="selected"</#if>>
                ${entity.name}
            </option>
        </#list>
    </select>
</#macro>

<#macro scaffoldIndex entityName entityNames resourceName entities cols>
    <@page title="${entityNames}">
        <br>
        <div>
            <@entitySearch entityName=entityNames resourceName=resourceName />
            <@entityListActionBar entityName=entityName resource=resourceName />
            <@entityTable entities=entities cols=cols selectionRole="selectObjectFromIndex" />
            <@paginator />
        </div>
    </@page>
</#macro>

<#macro scaffoldObjectChooser entityName entityNames resourceName entities cols>
        <br>
        <div>
            <@objectChooserSearch entityName=entityNames resourceName=resourceName />
<#--            <@entityListActionBar entityName=entityName resource=resourceName />-->
            <br />
            <@entityTable entities=entities cols=cols selectionRole="selectObjectFromObjectChooser" />
            <@paginator isObjectChooser=true />
        </div>
</#macro>