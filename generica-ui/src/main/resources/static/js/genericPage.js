
var objectChooserSelection = {};
var indexSelectorSelection = new Set();

const updateSelectAllCheckbox = function(selectAllCheckbox, forCheckboxes) {
    let allSelected = true;
    forCheckboxes.each(function() {
        allSelected = allSelected && $(this).prop("checked");
    });
    selectAllCheckbox.prop("checked", allSelected);
}

const renderObjectChooser = function (objChooserElem, formId, query, fromUrl) {
    let objChooser = $(objChooserElem);
    let res = objChooser.data("res");
    let target = objChooser.data("target");

    $.ajax({
        url: !!fromUrl ? fromUrl : "/" + res + "/objectChooser?form=" + formId + (query ? "&q=" + query : "") + "&selected=" + $("#" + target).val(),
        cache: false
    })
        .done(function (html) {
            objChooserElem.innerHTML = html;
            $(function() {
                $(".selectObjectFromObjectChooser", objChooser)
                    .on("change", function (e) {
                        if(this.checked)
                            objectChooserSelection[res].add(""+$(this).data("id"));
                        else
                            objectChooserSelection[res].delete(""+$(this).data("id"));

                        $("#" + target).val(Array.from(objectChooserSelection[res]).join(","));
                        updateSelectAllCheckbox($(".selectorSelectAll", objChooser), $(".selectObjectFromObjectChooser", objChooser));
                    });

                $(".selectorSelectAll").on("change", function(e) {
                    $(".selectObjectFromObjectChooser", objChooser).prop("checked", this.checked).change();
                });

                $(".objectChooserPageLink", objChooser).each(function() {
                    let url = $(this).prop("href");
                    $(this).prop("href", "#");
                    $(this).on("click", function(e) {
                        e.preventDefault();
                        renderObjectChooser(objChooserElem, formId, query, url);
                        return false;
                    });
                });

                updateSelectAllCheckbox($(".selectorSelectAll", objChooser), $(".selectObjectFromObjectChooser", objChooser));
            })
        })
};

$(function() {
    $(".objectChooser").each(function() {
        let formId = $(this).data("res") + "_form";
        let objectChooser = this;
        let targetValues = document.getElementById($(this).data("target")).value;
        let values = targetValues !== "" ? targetValues.split(",") : [];
        objectChooserSelection[$(this).data("res")] = new Set(values);
        $("#formContainer").append("<form id=\""+formId+"\" class=\"objectChooserForm\"></form>")
        renderObjectChooser(objectChooser, formId);
        $("#" + formId).on("submit", function (e) {
            e.preventDefault();
            renderObjectChooser(objectChooser, formId, $(this).serializeArray()[0].value);
            return false;
        });
    });
});

const updateButtonEnabledStates = function() {
    $(".selectionSensitiveButton").each(function() {
        let fb = "return " + $(this).data("enabledwhen");
        let disabled =  ! new Function("s", fb).apply(this, [indexSelectorSelection]);
        $(this).prop("disabled", disabled);
    })
}

$(function() {
    let indexSelector = $(".indexSelector");
    $(".selectObjectFromIndex", indexSelector).on("change", function (e) {
        if(this.checked)
            indexSelectorSelection.add(""+$(this).data("id"));
        else
            indexSelectorSelection.delete(""+$(this).data("id"));

       updateButtonEnabledStates();
       updateSelectAllCheckbox($(".selectorSelectAll", indexSelector), $(".selectObjectFromIndex", indexSelector));
    }).change();
    updateButtonEnabledStates();
    $(".selectorSelectAll").on("change", function(e) {
        $(".selectObjectFromIndex", indexSelector).prop("checked", this.checked).change();
    });
    $(".selectionSensitiveButton").on("click", function(e) {
       new Function("s", $(this).data("dowithselection")).apply(this, [indexSelectorSelection]);
    });
})